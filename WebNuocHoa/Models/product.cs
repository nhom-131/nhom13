namespace WebNuocHoa.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("product")]
    public partial class product
    {
        [Key]
        public int proid { get; set; }

        [StringLength(50)]
        public string proname { get; set; }

        public decimal? price { get; set; }

        [StringLength(50)]
        public string stock { get; set; }

        [StringLength(100)]
        public string images { get; set; }

        [StringLength(100)]
        public string descriptions { get; set; }

        public int catid { get; set; }

        public virtual category category { get; set; }
    }
}
